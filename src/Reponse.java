import java.io.Serializable;
import java.util.ArrayList;

public class Reponse implements Serializable {
	private String valeur;
	private ArrayList<Character> indices;

	/**
	 * Constructeur
	 * @param valeur valeur de la réponse
	 */
	public Reponse(String valeur, ArrayList<Character> indices) {
		this.valeur = valeur;
		this.indices = indices;
	}

	public String getValeur() {
		return valeur;
	}

	public ArrayList<Character> getIndices() {
		return indices;
	}

	public void addIndice(Character indice) {
		indices.add(indice);
	}
}
