import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class BdQuestionsReponses implements Serializable {

	private Noeud noeudPremier;
	private InfoJeu infoJeu;
	private ArrayList<Character> indicesJoueur;
	private QuestionCollection questions;
	private Reponse[] reponses;

	/**
	 * Constructeur
	 */
	public BdQuestionsReponses() {
		noeudPremier = null;
		infoJeu = null;
		questions = new QuestionCollection();
		indicesJoueur = new ArrayList<>();
		reponses = new Reponse[0];
	}

	public boolean reponseExiste(String valeur) {
		return trouverReponse(valeur) != null;
	}

	public boolean reponseExiste(ArrayList<Character> indices) {
		return trouverReponse(indices) != null;
	}

	public Reponse trouverReponse(String valeur) {
		Reponse laReponse = null;
		for(Reponse uneReponse : reponses) {
			if(uneReponse.getValeur().equals(valeur))
				laReponse = uneReponse;
		}
		return laReponse;
	}

	public Reponse trouverReponse(ArrayList<Character> indicesRep) {
		int longueurIndices = indicesRep.size();

		for(Reponse uneReponse : reponses) {
			ArrayList<Character> indicesBd = uneReponse.getIndices();
			// Sauter si longueur ne sont pas pareils
			if(longueurIndices != indicesBd.size())
				continue;

			Iterator iterJoueur = indicesRep.iterator();
			Iterator iterRep = indicesBd.iterator();
			boolean estPareil = true;
			while(iterRep.hasNext() && estPareil) {
				estPareil = iterRep.next().equals(iterJoueur.next());
			}
			if(estPareil)
				return uneReponse;
		}
		return null;
	}

//	public Character trouverBonIndice(String reponse) {
//		return
//	}
//
//	public int trouverPosMauvaisIndice(String reponse) {
//
//	}

	/**
	 * Ajoute une nouvelle réponse au BD
	 * @param reponse Valeur de la réponse
	 * @param indices Indices de la réponse
	 */
	public void addReponse(String reponse, ArrayList<Character> indices) {
		Reponse[] reponsesX = new Reponse[reponses.length + 1];

		int i = 0;

		// On remplit les réponses avec les vielles
		for(Reponse uneReponse: reponses)
			reponsesX[i++] = uneReponse;

		// On ajoute la nouvelle réponse
		reponsesX[i] = new Reponse(reponse, indices);

		reponses = reponsesX;
	}

	/**
	 * Retourne si la bd est vide
	 * @return true si noeudActuel est nulle
	 */
	public boolean estVide() {
		return noeudPremier == null;
	}

	/**
	 * Met le premier noeud de l'arbre
	 */
	public void choisirPremiereQuestion() {
		infoJeu.noeudCourant = noeudPremier;
		infoJeu.dernierIndice = false;
		infoJeu.noeudPrecedant = null;
		indicesJoueur = new ArrayList<>();
	}

	/**
	 * Retourne si le noeud courant est une réponse
	 * @return true si les options de QuestionActuel sont nulles
	 */
	public boolean reponseTrouvee() {
		return infoJeu.noeudCourant == null;
		//return infoJeu.noeudCourant.getGauche() == null && infoJeu.noeudCourant.getDroite() == null;
	}

	/**
	 * Retourne la chaine du noeud
	 * @return String valeur actuel
	 */
	public String getLaChaineActuelle() {
		if(reponseTrouvee())
			return trouverReponse(indicesJoueur).getValeur();
		else
			return questions.getValeurAt(infoJeu.noeudCourant.getPosition());
	}

	/**
	 * Ajoute une question/réponse
	 * @param reponse Réponse pensée
	 * @param question Question pour la question
	 */
	public void ajoutQR(String reponse, String question) {
		// Est-ce qu'il a répondu non à une réponse
		if(infoJeu != null) {
			Noeud noeudPrecedant = infoJeu.noeudPrecedant;
			boolean optionPrec = infoJeu.dernierIndice;
			Noeud nouveauNoeud = new Noeud(questions.enfile(question));

			if (optionPrec)
				noeudPrecedant.setGauche(nouveauNoeud);
			else
				noeudPrecedant.setDroite(nouveauNoeud);

			if(reponseExiste(indicesJoueur)) {
				Reponse mauvaiseReponse = trouverReponse(indicesJoueur);
				mauvaiseReponse.addIndice(Constantes.REPONSE_NEGATIVE);
			}

		} else {
			infoJeu = new InfoJeu();
			noeudPremier = new Noeud(questions.enfile(question));
		}
		addReponse(reponse, indicesJoueur);
		// Ajout indice positif à la nouvelle réponse
		reponses[reponses.length - 1].addIndice(Constantes.REPONSE_POSITIVE);

	}

	/**
	 * Se déplace dans l'arbre
	 * @param reponse Option choisie (gauche ou droite)
	 * @return true si resteQuestion
	 */
	public boolean deplacerDansArbre(int reponse) {
		infoJeu.noeudPrecedant = infoJeu.noeudCourant;
		if(reponse == 0) {
			infoJeu.dernierIndice = true;
			infoJeu.noeudCourant = infoJeu.noeudCourant.getGauche();
			indicesJoueur.add(Constantes.REPONSE_POSITIVE);

		} else {
			infoJeu.dernierIndice = false;
			infoJeu.noeudCourant = infoJeu.noeudCourant.getDroite();
			indicesJoueur.add(Constantes.REPONSE_NEGATIVE);
		}
		return reponseExiste(indicesJoueur) || infoJeu.noeudCourant != null;
	}


}
