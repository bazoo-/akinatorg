import java.io.Serializable;

public class Noeud implements Serializable {

	private int position;
	private Noeud gauche, droite;

	public Noeud(int position) {
		this.position = position;
	}

	public int getPosition() {
		return position;
	}

	public Noeud getGauche() {
		return gauche;
	}

	public Noeud getDroite() {
		return droite;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setGauche(Noeud gauche) {
		this.gauche = gauche;
	}

	public void setDroite(Noeud droite) {
		this.droite = droite;
	}
}