/**
 * Constante du projet de jeu du divinateur (voir �nonc� tp2 h1017).
 *
 * @author pbelisle
 * @version hiver 2017
 */
public class Constantes {

	// Caract�re pour retenir les indices donn�es par l'utilisateur.
	public static final char REPONSE_POSITIVE = 'O';
	public static final char REPONSE_NEGATIVE = 'N';

	// Nom de fichier par d�faut pour retenir la bd.
	public static final String NOM_FICHIER_BD = "bd.bin";
	public static final String NOM_FICHIER_BD2 = "bd2.bin";

	public static final String AVOUER_RIEN = "Je ne connais rien. ";
	public static final String AVOUER_TROUVER = "Je n'ai pas trouvé votre réponse. ";
	public static final String DEMANDE_QUESTION = "Entrez une question qui distingue ";
	public static final String DEMANDE_REPONSE = "Entrez ce à quoi vous pensiez.";

}
