import java.io.Serializable;

public class QuestionCollection implements Serializable {

	private Question tete;
	private int longueur;

	public QuestionCollection() {
		tete = null;
		longueur = 0;
	}

	public int size() {
		return longueur;
	}

	public boolean estVide() {
		return this.tete == null;
	}

	/**
	 * Ajoute
	 * @param valeur
	 * @return
	 */
	public int enfile(String valeur) {

		for(int i=0; i < longueur; i++) {
			if(getValeurAt(i).equals(valeur)) return i; // Si question existe déja, on retourne la position
			i++;
		}

		Question q = construireQuestion(valeur);
		getQuestionAt(longueur - 1).setSuivant(q); // Ajoute à la dernière question
		return longueur++;
	}

	private Question construireQuestion(String valeur) {
		Question q = new Question(valeur);
		tete = tete != null ? tete : q;
		return q;
	}

	public String defile() {
		Question questionDeRetour = this.tete;
		this.tete = this.tete.getSuivant();
		longueur--;
		return questionDeRetour.getValeur();
	}

	public String getValeurAt(int index) {
		return getQuestionAt(index).getValeur();
	}

	public Question getQuestionAt(int index) {
		Question curseur = tete;
		int i = 0;
		while(curseur.getSuivant() != null && i < index) {
			curseur = curseur.getSuivant();
			i++;
		}
		return curseur;
	}

	private class Question implements Serializable{

		private String valeur;
		private Question suivant;

		protected Question(String valeur) {
			this.valeur = valeur;
		}

		protected String getValeur() {
			return valeur;
		}

		protected void setValeur(String valeur) {
			this.valeur = valeur;
		}

		protected Question getSuivant() {
			return suivant;
		}

		protected void setSuivant(Question suivant) {
			this.suivant = suivant;
		}

	}

}
